<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210818112801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE interet_user DROP FOREIGN KEY FK_940627F6C1289ECC');
        $this->addSql('CREATE TABLE enseignement (id INT AUTO_INCREMENT NOT NULL, compétence VARCHAR(255) NOT NULL, interet VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE enseignement_user (enseignement_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_C071A6B5ABEC3B20 (enseignement_id), INDEX IDX_C071A6B5A76ED395 (user_id), PRIMARY KEY(enseignement_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE enseignement_user ADD CONSTRAINT FK_C071A6B5ABEC3B20 FOREIGN KEY (enseignement_id) REFERENCES enseignement (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE enseignement_user ADD CONSTRAINT FK_C071A6B5A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE interet');
        $this->addSql('DROP TABLE interet_user');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE enseignement_user DROP FOREIGN KEY FK_C071A6B5ABEC3B20');
        $this->addSql('CREATE TABLE interet (id INT AUTO_INCREMENT NOT NULL, interet VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE interet_user (interet_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_940627F6A76ED395 (user_id), INDEX IDX_940627F6C1289ECC (interet_id), PRIMARY KEY(interet_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE interet_user ADD CONSTRAINT FK_940627F6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE interet_user ADD CONSTRAINT FK_940627F6C1289ECC FOREIGN KEY (interet_id) REFERENCES interet (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE enseignement');
        $this->addSql('DROP TABLE enseignement_user');
    }
}
