<?php

namespace App\Controller;
use App\Entity\User;
use App\Entity\Enseignement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class SecurityController extends AbstractController
{
  public function __construct(UserPasswordHasherInterface $passwordHasher)
     {
        $this->passwordHasher = $passwordHasher;
     }
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
/**
* @Route("/profile", name="home")
*/
public function index(AuthenticationUtils $authenticationUtils){

$compétence = $this->getUser()->getEnseignements();
$lastUsername = $authenticationUtils->getLastUsername();
//dd($compétence);

return $this->render('Profile/profile.html.twig', [
           'lastUsername' => $lastUsername,
            'compétence' => $compétence
        ]);
}

/**
* @Route("/inscription", name="inscription")
*/
public function inscription(Request $request):Response
{

$user = new User();

$form = $this->createFormBuilder($user)
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('save', SubmitType::class, ['label' => 'Create User'])
            ->getForm();


$form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
        $form->getData();



$user->setEmail($form['email']->getData());
$user->setRoles(['ROLE_USER']);
$user->setPassword($this->passwordHasher->hashPassword( $user, $form['password']->getData()));

             $entityManager = $this->getDoctrine()->getManager();
             $entityManager->persist($user);
             $entityManager->flush();

}

            //return $this->redirectToRoute('');
        
  return $this->render('security/inscription.html.twig', [
            'form' => $form->createView(),
        ]);
}
}
