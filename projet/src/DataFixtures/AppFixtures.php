<?php

namespace App\DataFixtures;
use App\Entity\User;
use App\Entity\Enseignement;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
 use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
class AppFixtures extends Fixture
{
 private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
     {
        $this->passwordHasher = $passwordHasher;
     }
    public function load(ObjectManager $manager)
    {
 $user = new User();
          // ...
           for ($i = 0; $i < 20; $i++) {
            $user = new User();
            $user->setEmail('email@00'.$i);
            $user->setRoles(['ROLE_USER']);
            $user->setPassword($this->passwordHasher->hashPassword(
            $user,
             'the_new_password'
        ));
$Enseignement = new Enseignement();

$Enseignement->setCompétence('francais'.$i);
$Enseignement->setInteret('informatique'.$i);
$Enseignement->addUser($user);
 $manager->persist($user);
 $manager->persist($Enseignement);
        }

        $manager->flush();

    }
}
